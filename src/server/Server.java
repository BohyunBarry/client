package server;

import java.io.Closeable;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;


import com.sun.istack.internal.NotNull;

import info.Account;
import info.AccountInfo;
import info.ChatMessage;
import info.ChatRequest;
import info.ChatResult;
import info.RegisterInfo;
import server.Worker.Authenticated;
import server.Worker.RequestReceived;
import security.Log;
import security.Security;
import security.StreamUtilities;
import security.Task;

public final class Server extends javax.swing.JFrame implements Closeable, Authenticated {
	private ServerSocket serverSocket;
	private List<Worker> workers = new LinkedList<Worker>();
	private Object lock = new Object();
	private RequestHandler requestHandler = new RequestHandler();
	
	  private javax.swing.JButton jButton1;
	  private javax.swing.JButton jButton2;
	  private javax.swing.JLabel jLabel3;
	  private javax.swing.JScrollPane jScrollPane1;
	  public javax.swing.JTextArea jTextArea1;
	  private javax.swing.JTextField jTextField3;

	

	
	public Server(int port, String address, int maxConnection) throws IOException {
		Log.i("Server starting...");
		serverSocket = new ServerSocket(port, maxConnection, InetAddress.getByName(address));
		Log.i("Server started...");
	}

	public void waitForConnection() throws IOException {
		Log.i("Waiting for connecting...");
		Socket socket = null;
		while ((socket = serverSocket.accept()) != null) {
			Log.i("Accepted " + socket.getRemoteSocketAddress().toString());
			talkWithClient(socket);
		}
	}

	private Worker createWorker(Socket socket) {
		Worker worker = null;
		try {
			Log.i("Creating worker...");
			worker = new Worker(socket);
		} catch (IOException | SecurityException e) {
			Log.i("Creating was aborted!");
		}

		if (worker != null) {
			synchronized (lock) {
				workers.add(worker);
			}
		} else {
			StreamUtilities.tryCloseStream(socket);
		}
		return worker;
	}

	private void notifyAllWorker(Worker broadcastWorker) {
		synchronized (lock) {
			ChatResult result = new ChatResult();
			result.setCode(ChatResult.CODE_OK);
			result.setRequestCode(ChatRequest.CODE_FRIEND_STATE);
			result.setExtra(broadcastWorker.getAccount());
			for (int i = 0; i < workers.size(); i++) {
				Worker friendWorker = workers.get(i);
				if (!broadcastWorker.equals(friendWorker)) {
					try {
						friendWorker.response(result);
					} catch (IOException e) {
						killWorker(friendWorker);
					}
				}
			}
		}
	}

	private void killWorker(Worker worker) {
		worker.release();
		worker.setReceivedData(null);
		worker.setAuthenticated(null);
		synchronized (lock) {
			workers.remove(worker);
		}
		if (worker.getAccount() != null) {
			Log.i("Removed worker " + worker.getAccount().getAccountId());
			worker.getAccount().setState(AccountInfo.STATE_OFFLINE);
			notifyAllWorker(worker);
		} else {
			Log.i("Removed anonymous worker");
		}
	}

	private void talkWithClient(final Socket socket) {
		Task.run(new Runnable() {
			@Override
			public void run() {
				Worker worker = createWorker(socket);
				if (worker != null) {
					try {
						Log.i("Start worker bridge!");
						worker.setAuthenticated(Server.this);
						worker.setReceivedData(requestHandler);
						worker.startBridge();
					} catch (IOException e) {
					}
					killWorker(worker);
				} else {
					Log.i("Createing worker: unsuccessful");
				}
			}
		});
	}

	@Override
	public void close() throws IOException {
		serverSocket.close();
		while (!workers.isEmpty()) {
			Worker worker = workers.get(0);
			killWorker(worker);
		}
	}

	@Override
	public void onAuthenticated(Worker worker) {
		Log.i("Broadcast new worker!");
		notifyAllWorker(worker);
	}
	
	
	
	
	private class RequestHandler implements RequestReceived {						
		@Override
		@NotNull
		public ChatResult onRequestReceived(Worker sender, ChatRequest request) {
			ChatResult responseObject = null;
			switch (request.getCode()) {
			case ChatRequest.CODE_FRIENDS_LIST:
				responseObject = responseFriendsList(sender);
				break;
			case ChatRequest.CODE_MY_ACCOUNT_INFO:
				responseObject = responseAccountInfo(sender);
				break;
			case ChatRequest.CODE_CHAT_MESSAGE:
				responseObject = forwardChatMessage(sender,
						request.getExtra() instanceof ChatMessage ? (ChatMessage) request.getExtra() : null);
				break;
			case ChatRequest.CODE_LOGIN:
				responseObject = responseLoginResult(sender,
						request.getExtra() instanceof Account ? (Account) request.getExtra() : null);
				break;
			case ChatRequest.CODE_CHANGE_DISPNAME:
				responseObject = responseChangeDisplayName(sender.getAccount(),
						request.getExtra() != null ? request.getExtra().toString() : null);
				notifyAllWorker(sender);
				break;
			case ChatRequest.CODE_CHANGE_PASSWORD:
				responseObject = responseChangePassword(sender.getAccount().getAccountId(),
						request.getExtra() != null ? request.getExtra().toString() : null);
				break;
			case ChatRequest.CODE_CHANGE_STATUS:
				responseObject = responseChangeStatus(sender.getAccount(),
						request.getExtra() != null ? request.getExtra().toString() : null);
				notifyAllWorker(sender);
				break;
			case ChatRequest.CODE_REGISTER:
				if (request.getExtra() instanceof RegisterInfo) {
					RegisterInfo registerInfo = (RegisterInfo) request.getExtra();
					responseObject = responseRegister(registerInfo.getUsername(), registerInfo.getPassword(),
							registerInfo.getDisplayName());
				}
				break;
			}
			responseObject.setRequestCode(request.getCode());
			return responseObject;
		}

		private boolean isLogged(AccountInfo accountInfo) {
			if (accountInfo == null)
				return false;
			synchronized (lock) {
				for (Worker worker : workers) {
					AccountInfo existsAccountInfo = worker.getAccount();
					if (existsAccountInfo != null && existsAccountInfo.getAccountId() == accountInfo.getAccountId()) {
						return true;
					}
				}
				return false;
			}
		}

		private ChatResult responseLoginResult(Worker sender, Account account) {
			if (account == null)
				return null;
			AccountInfo accountInfo = AccountManager.getInstance().getAccountInfo(account.getUsername(),
					Security.getPasswordHash(account.getPassword()));
			ChatResult result = new ChatResult();
			boolean logged = isLogged(accountInfo);
			result.setCode(ChatResult.CODE_FAIL);
			if (accountInfo != null) {
				if (logged) {
					result.setExtra("already logged in other place");
					Log.i("Response login: FAIL - already logged in other place");
				} else {
					result.setCode(ChatResult.CODE_OK);
					sender.setAccount(accountInfo);
					Log.i("Response login: OK");
				}
			} else {
				result.setExtra("Wrong username or password!");
				Log.i("Response login: FAIL - wrong username or password");
			}
			return result;
		}

		private ChatResult responseAccountInfo(Worker sender) {
			Log.i("Request account info for " + sender.getAccount().getAccountId());
			ChatResult result = new ChatResult();
			result.setCode(ChatResult.CODE_OK);
			result.setExtra(sender.getAccount());
			return result;
		}


		private ChatResult responseFriendsList(Worker sender) {
			Log.i("Request all friends for " + sender.getAccount().getAccountId());
			List<AccountInfo> allFriends = AccountManager.getInstance().getAllAccountInfos();
			int exceptId = sender.getAccount().getAccountId();
			AccountInfo exceptAccount = null;
			for (AccountInfo friend : allFriends) {
				if (friend.getAccountId() == exceptId) {
					exceptAccount = friend;
					continue;
				}
				for (Worker worker : workers) {
					if (worker.getAccount().equals(friend)) {
						friend.setState(AccountInfo.STATE_ONLINE);
					}
				}
			}
			allFriends.remove(exceptAccount);
			ChatResult result = new ChatResult();
			result.setCode(ChatResult.CODE_OK);
			result.setExtra(allFriends);
			return result;
		}

		private ChatResult forwardChatMessage(Worker sender, ChatMessage chatMessage) {
			if (chatMessage == null)
				return null;
			Log.l(String.format("%d > %d: %s", sender.getAccount().getAccountId(), chatMessage.getWhoId(),
					chatMessage.getContent()));

			int whoReceiverId = chatMessage.getWhoId();
			Worker whoReceiver = null;

			chatMessage.setWhoId(sender.getAccount().getAccountId());

			synchronized (lock) {
				for (Worker _receiver : workers) {
					if (_receiver.getAccount().getAccountId() == whoReceiverId) {
						whoReceiver = _receiver;
						break;
					}
				}
			}

			ChatResult result = new ChatResult();
			result.setCode(ChatResult.CODE_FAIL);
			if (whoReceiver != null) {
				try {
					ChatResult forwardResult = new ChatResult();
					forwardResult.setCode(ChatResult.CODE_OK);
					forwardResult.setRequestCode(ChatRequest.CODE_CHAT_MESSAGE);
					forwardResult.setExtra(chatMessage);
					whoReceiver.response(forwardResult);
					result.setCode(ChatResult.CODE_OK);
				} catch (IOException e) {
					// e.printStackTrace();
					result.setExtra("Friend's connection broken down!");
				}
			} else {
				result.setExtra("Friend was offline!");
			}
			return result;
		}

		private ChatResult responseRegister(String username, String password, String dispname) {
			Log.i(String.format("Register: %s", username));
			ChatResult result = new ChatResult();
			result.setCode(ChatResult.CODE_FAIL);
			if (Security.checkValidUsername(username) && Security.checkValidPassword(password)
					&& Security.checkValidDisplayName(dispname)) {
				username = username.trim();
				String passhash = Security.getPasswordHash(password);
				dispname = dispname.trim();
				AccountManager.getInstance().addAccount(username, passhash, dispname);
				result.setCode(ChatResult.CODE_OK);
			}
			return result;
		}

		private ChatResult responseChangeDisplayName(AccountInfo account, String dispname) {
			Log.i(String.format("Change display name %d to %s", account.getAccountId(), dispname));
			ChatResult result = new ChatResult();
			result.setCode(ChatResult.CODE_FAIL);
			result.setExtra(account);
			if (Security.checkValidDisplayName(dispname)) {
				dispname = dispname.trim();
				account.setDisplayName(dispname);
				AccountManager.getInstance().changeDisplayName(account.getAccountId(), dispname);
				result.setCode(ChatResult.CODE_OK);
			}
			return result;
		}

		private ChatResult responseChangeStatus(AccountInfo account, String status) {
			Log.i(String.format("Change status %d to %s", account.getAccountId(), status));
			ChatResult result = new ChatResult();
			result.setCode(ChatResult.CODE_FAIL);
			result.setExtra(account);
			if (status != null && (status = status.trim()).length() > 0) {
				account.setStatus(status);
				AccountManager.getInstance().changeStatus(account.getAccountId(), status);
				result.setCode(ChatResult.CODE_OK);
			}
			return result;
		}

		private ChatResult responseChangePassword(int id, String password) {
			Log.i(String.format("Change password %d", id));
			ChatResult result = new ChatResult();
			result.setCode(ChatResult.CODE_FAIL);
			if (Security.checkValidPassword(password)) {
				AccountManager.getInstance().changePasswordHash(id, Security.getPasswordHash(password));
				result.setCode(ChatResult.CODE_OK);
			}
			return result;
		}
	}
	
	 private void initComponents() {

	        jButton1 = new javax.swing.JButton();
	        jScrollPane1 = new javax.swing.JScrollPane();
	        jTextArea1 = new javax.swing.JTextArea();
	        jLabel3 = new javax.swing.JLabel();
	        jTextField3 = new javax.swing.JTextField();
	        jButton2 = new javax.swing.JButton();

	        //setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
	        //setTitle("jServer");

	        jButton1.setText("Start Server");
	        jButton1.setEnabled(false);
	        //jButton1.addActionListener(new java.awt.event.ActionListener() {
	           // public void actionPerformed(java.awt.event.ActionEvent evt) {
	               // jButton1ActionPerformed(evt);
	            //}
	        //});

	        jTextArea1.setColumns(20);
	        jTextArea1.setFont(new java.awt.Font("Consolas", 0, 12)); // NOI18N
	        jTextArea1.setRows(5);
	        jScrollPane1.setViewportView(jTextArea1);

	        /*
	        jLabel3.setText("Database File : ");

	        jButton2.setText("Browse...");
	        jButton2.addActionListener(new java.awt.event.ActionListener() {
	            public void actionPerformed(java.awt.event.ActionEvent evt) {
	                jButton2ActionPerformed(evt);
	            }
	        });
	        */

	        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
	        getContentPane().setLayout(layout);
	        layout.setHorizontalGroup(
	            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(layout.createSequentialGroup()
	                .addContainerGap()
	                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                    .addComponent(jScrollPane1)
	                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
	                        .addComponent(jLabel3)
	                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
	                        .addComponent(jTextField3, javax.swing.GroupLayout.DEFAULT_SIZE, 282, Short.MAX_VALUE)
	                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
	                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                        .addComponent(jButton1)))
	                .addContainerGap())
	        );
	        layout.setVerticalGroup(
	            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(layout.createSequentialGroup()
	                .addContainerGap()
	                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
	                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                    .addComponent(jLabel3)
	                    .addComponent(jButton2)
	                    .addComponent(jButton1))
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
	                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 287, Short.MAX_VALUE)
	                .addContainerGap())
	        );

	        pack();
	    }
	 /*
	 private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {
	        server = new SocketServer(this);
	        jButton1.setEnabled(false); jButton2.setEnabled(false);
	    }

	    public void RetryStart(int port){
	        if(server != null){ server.stop(); }
	        server = new SocketServer(this, port);
	    }
	    
	   
	    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
	        fileChooser.showDialog(this, "Select");
	        File file = fileChooser.getSelectedFile();
	        
	        if(file != null){
	            filePath = file.getPath();
	            if(this.isWin32()){ filePath = filePath.replace("\\", "/"); }
	            jTextField3.setText(filePath);
	            jButton1.setEnabled(true);
	        }
	    }
	    */

	 public Server() {
	        initComponents();     
	        jTextField3.setEditable(false);
	        jTextArea1.setEditable(false);
	    }
	 
	 
 	private static void setSystemLookAndFeel() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e) {
		}
	}
	 
	    public static void main(String args[]) {

	        try{
	            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	        }
	        catch(Exception ex){
	            System.out.println("Look & Feel Exception");
	        }
	        
	        java.awt.EventQueue.invokeLater(new Runnable() {
	            public void run() {
	                new Homechat().setVisible(true);
	            }
	        });
	    }
}
