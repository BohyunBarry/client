package server;

import java.io.IOException;
import java.net.Socket;

import com.sun.istack.internal.NotNull;

import info.AccountInfo;
import info.ChatRequest;
import info.ChatResult;
import transmission.ITransmission;
import transmission.ObjectAdapter;
import transmission.Control;
import transmission.SocketTransmission;
import security.StreamUtilities;

public class Worker {
	private AccountInfo myAccount = null;
	private ITransmission transmission;
	private ObjectAdapter objectAdapter;
	private Control control;
	private RequestReceived requestreceived = null;
	private Authenticated authenticated = null;

	public void setReceivedData(RequestReceived r1) {
		requestreceived = r1;
	}
	
	public void setAuthenticated(Authenticated a1) {
		authenticated = a1;
	}

	public void response(@NotNull ChatResult result) throws IOException {
		control.sendObject(result);
	}

	public void startBridge() throws IOException {
		while (true) {
			Object receivedObject = control.receiveObject();
			if (receivedObject == null)
				break;
			if (receivedObject instanceof ChatRequest) {
				if (requestreceived != null)
					response(requestreceived.onRequestReceived(this, (ChatRequest) receivedObject));
			}
		}
	}

	public void release() {
		StreamUtilities.tryCloseStream(transmission);
	}

	public Worker(Socket socket) throws IOException {
		transmission = new SocketTransmission(socket);
		objectAdapter = new ObjectAdapter();
		control = new Control(objectAdapter, transmission);
	}
	
	public void setAccount(AccountInfo accountInfo) {
		this.myAccount = accountInfo;
		if (accountInfo != null && authenticated != null)
			authenticated.onAuthenticated(this);
	}

	public AccountInfo getAccount() {
		return myAccount;
	}

	public interface Authenticated {
		void onAuthenticated(Worker worker);
	}
	
	public interface RequestReceived {
		@NotNull
		ChatResult onRequestReceived(Worker sender, ChatRequest request);
	}
}
