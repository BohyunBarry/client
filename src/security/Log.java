package security;

import com.sun.istack.internal.NotNull;

public final class Log {
	private static Logable logable = new StdLogable();

	Log() {
	}

	public static void i(Object obj) {
		logable.i(obj);
	}

	public static void l(Object obj) {
		logable.l(obj);
	}

	public static void setLogable(@NotNull Logable lo) {
		logable = lo;
	}

	public static Logable getLogable() {
		return logable;
	}

	private static class StdLogable implements Logable {

		@Override
		public void i(Object obj) {
			System.out.println(obj != null ? obj.toString() : "NULL");
		}

		@Override
		public void l(Object obj) {
			System.out.println(obj != null ? obj.toString() : "NULL");
		}

	}

	public interface Logable {
		void i(Object obj);

		void l(Object obj);
	}
}
