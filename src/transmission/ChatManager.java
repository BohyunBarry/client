package transmission;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import javax.imageio.ImageIO;

public final class ChatManager {
	private static ChatManager chatmanager;

	public static ChatManager getChatmanager() {
		if (chatmanager == null)
			chatmanager = new ChatManager();
		return chatmanager;
	}

	public static void destroyChatmanager() {
		if (chatmanager != null) {
			chatmanager.release();
			chatmanager = null;
		}
	}

	private HashMap<String, BufferedImage> imagesHolder = new HashMap<>();

	private void release() {
		for (BufferedImage image : imagesHolder.values()) {
			image.flush();
		}
		imagesHolder.clear();
	}

	private BufferedImage loadImage(String name) throws IOException {
		InputStream imageStream = getClass().getResourceAsStream("/" + name);
		BufferedImage image = ImageIO.read(imageStream);
		imagesHolder.put(name, image);
		return image;
	}

	public BufferedImage getImageByName(String name) {
		try {
			return imagesHolder.containsKey(name) ? imagesHolder.get(name) : loadImage(name);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
