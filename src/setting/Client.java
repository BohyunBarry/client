package setting;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import com.sun.istack.internal.NotNull;

import info.Account;
import info.ChatRequest;
import info.ChatResult;
import transmission.ITransmission;
import transmission.ObjectAdapter;
import transmission.Control;
import transmission.SocketTransmission;
import security.Log;
import security.StreamUtilities;
import security.Task;

public final class Client {
	private static Client client = null;
	private ITransmission transmission;
	private ObjectAdapter objectAdapter;
	private Control control;
	private Account myAccount = new Account();
	private List<DataReceived> dataReceived = new ArrayList<>();
	private ConnectionHasProblem connectionHasProblem = null;
	private boolean Stop = false;

	public void addDataReceived(DataReceived dr) {
		dataReceived.add(dr);
	}

	public void removeDataReceived(DataReceived dr) {
		dataReceived.remove(dr);
	}

	public void setConnectionHasProblem(ConnectionHasProblem p) {
		connectionHasProblem = p;
	}

	public static Client getClient() {
		return client;
	}

	public static void createClient(String serverAddress, int serverPort) throws UnknownHostException, IOException {
		destroyClient();
		client = new Client(serverAddress, serverPort);
	}

	public static void destroyClient() {
		if (client != null) {
			client.release();
			client = null;
		}
	}

	private void release() {
		Stop = true;
		StreamUtilities.tryCloseStream(transmission);
	}
	
	public String getRemoteAddress() {
		if (transmission instanceof SocketTransmission)
			return ((SocketTransmission) transmission).getSocket().getRemoteSocketAddress().toString();
		return "somewhere";
	}

	public void startLooper() {
		Task.run(new Runnable() {
			@Override
			public void run() {
				boolean isCorruptData = false;
				do {
					try {
						Object receivedObject = control.receiveObject();
						if (receivedObject instanceof ChatResult) {
							for (int i = 0; i < dataReceived.size(); i++) {
								DataReceived DR = dataReceived.get(i);
								if (!DR.onDataReceived(Client.this, (ChatResult) receivedObject)) {
									isCorruptData = true;
									break;
								}
							}
						} else {
							isCorruptData = true;
							Log.i("Received data is NULL or corrupted");
						}
					} catch (IOException e) {
						if (!Stop && connectionHasProblem != null)
							connectionHasProblem.onConnectionHasProblem(e.getMessage());
					}
				} while (!isCorruptData && !Stop);
				if (isCorruptData)
					fireConnectionHasProblemEvent("Received data has been corrupted!");
			}
		});
	}

	private void fireConnectionHasProblemEvent(String message) {
		if (connectionHasProblem != null)
			connectionHasProblem.onConnectionHasProblem(message);
	}

	public void request(ChatRequest request) {
		try {
			control.sendObject(request);
		} catch (IOException e) {
			fireConnectionHasProblemEvent(e.getMessage());
		}
	}

	Client(String serverAddress, int serverPort) throws UnknownHostException, IOException {
		Socket socket = new Socket(serverAddress, serverPort);
		transmission = new SocketTransmission(socket);
		objectAdapter = new ObjectAdapter();
		control = new Control(objectAdapter, transmission);
	}
	
	public int getMyId() {
		return myAccount.getId();
	}
	
	public void setMyId(int id) {
		myAccount.setId(id);
	}
	
	public String getMyUsername() {
		return myAccount.getUsername();
	}

	public void setMyUsername(String myUsername) {
		myAccount.setUsername(myUsername);
	}

	public interface DataReceived {
		boolean onDataReceived(Client sender, @NotNull ChatResult receivedObject);
	}

	public interface ConnectionHasProblem {
		void onConnectionHasProblem(String message);
	}
}
