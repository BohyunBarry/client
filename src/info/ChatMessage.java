package info;

import java.io.Serializable;

public class ChatMessage implements Serializable {
	private static final long serialVersionUID = 5140826547264710686L;
	private int whoId;
	private String content;
	public String type, sender, recipient;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getWhoId() {
		return whoId;
	}

	public void setWhoId(int whoId) {
		this.whoId = whoId;
	}
	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}
	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}
	/*
    public ChatMessage(String type, String sender,String content, String recipient){
        this.type = type; 
        this.sender = sender;  
        this.recipient = recipient;
        this.content = content;
    }
    */
    public void setType(String type)
    {
    	this.type = type;
    }
    public String getType()
    {
    	return type;
    }
    
    @Override
    public String toString(){
        return "{type='"+type+"', sender='"+sender+"', content='"+content+"', recipient='"+recipient+"'}";
    }
}
