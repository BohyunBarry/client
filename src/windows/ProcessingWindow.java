package windows;

import javax.swing.SwingUtilities;

import info.ChatRequest;
import info.ChatResult;
import setting.Client;
import setting.Client.DataReceived;
import security.Task;

public abstract class ProcessingWindow extends Window implements DataReceived {
	private static final long serialVersionUID = -1863170957200543995L;
	private int requestCode;

	public ProcessingWindow() {
		if (Client.getClient() != null)
			Client.getClient().addDataReceived(this);
	}

	@Override
	protected void onWindowClosing() {
		super.onWindowClosing();
		if (Client.getClient() != null)
			Client.getClient().removeDataReceived(this);
	}

	protected abstract void doneBackgoundTask(ChatResult result);

	protected void doInBackground(ChatRequest request) {
		doInBackground(request, "Processing...");
	}

	protected void doInBackground(final ChatRequest request, String processingMessage) {
		this.requestCode = request.getCode();
		setVisible(false);
		ProcessingDialog.showBox(this, processingMessage);
		Task.run(new Runnable() {
			@Override
			public void run() {
				Client.getClient().request(request);
			}
		});
	}

	@Override
	public boolean onDataReceived(Client sender, final ChatResult receivedObject) {
		if (receivedObject.getRequestCode() == requestCode) {
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					ProcessingDialog.hideBox();
					doneBackgoundTask(receivedObject);
				}
			});

		}
		return true;
	}
}
